#icode
一个验证码机器识别程序。

 * Optical Character Recognizer.
 * <p>1.filter
 * <p>2.extractLines
 * <p>3.extractChars
 * <p>4.code
 * <p>5.store codes or recognize(greedyOcr or meanOcr.Best for meanOcr.).
 * <P>说明：本程序只可做学习使用，不可用于网络攻击和强力验证码爆破。学习和交流：<b>mobangjack@foxmail.com</b>
 * <p>鸣谢：Ronald B. Cemer
 * @mobangjack  帮杰
 
